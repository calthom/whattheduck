/**
 * UBC CPSC 314, April 2016
 * Project 4
 */

// GLOBAL VARIABLES
var container, scene, camera, renderer, raycaster, fireballRay;
var window_width, window_height;
var floor;
var keyboard = new THREEx.KeyboardState();
var mouse = new THREE.Vector2(), target;
var mouseDown = false;
var tDown = false;
var dirLight; 
var runOff = 250; 
var startTime = Date.now(); 

var y_vector_norm = new THREE.Vector3(0,1,0);
var x_vector_norm = new THREE.Vector3(1,0,0);
var z_vector_norm = new THREE.Vector3(0,0,1);

var  unlitMaterial, duckTexture, duckMaterial, transparentMaterial, floorMaterial, skyMaterial;

var lastTimeCheck, averageFPS;
 
var ducks = [];
var duckCount = 0;
var duckDestroyedCount = 0;


var fire = false;
var timeElapsed = 70; 
var fpscount; 
var fps; 
var totalFPS; 

var particlesGlobal ;
var numParticles = 20; 
var particleSystemGlobal; 
var duckCollisionBoxGeometry;
var loader = new THREE.FontLoader();
var winningDuckCount = 5; 
var numDucks = 10; 
var devilDuckAttack = false;  
var attackingDevilDuck = null;
var attackVector; 

var currWeapon = 'breadcrumbs';

var fireballs = [];
var fireballGeometry;
var earthNormalAtCamera;

var throwCrumbs = false;

var shaderFiles = [];

var exit = false;

var breadCrumbDuck; 

var youWon = false; 

init();

function init() {
    
        generateSound();

	scene = new THREE.Scene();

	// get height and width of screen
	window_width = window.innerWidth;
	window_height = window.innerHeight;

	// get viewing angle, aspect ratio, and values of near and far
	var view_angle = 45, aspect_ratio = window_width / window_height, near = 1, far = 10000;

	// set up camera
	camera = new THREE.PerspectiveCamera( view_angle, aspect_ratio, near, far );

	// place camera in scene
	camera.position.set(100,20,0);
	camera.lookAt(new THREE.Vector3(50,20,0));
	scene.add(camera)

	// SETUP RENDERER
	renderer = new THREE.WebGLRenderer();
	renderer.setSize( window.innerWidth, window.innerHeight );
	renderer.setClearColor(0xffffff);
	document.body.appendChild(renderer.domElement);

	raycaster = new THREE.Raycaster();
	
	
	// Map sky texture to inside of sphere and place world inside sphere
	skyMaterial	= THREE.ImageUtils.loadTexture('images/sky.jpg' );
	var sphereMaterial = new THREE.MeshBasicMaterial( {map: skyMaterial, side: THREE.BackSide});
	var sphereGeometry = new THREE.SphereGeometry(9000,16,16);
	var sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
	scene.add( sphere );

	// Add grassy plane to scene
	floorTexture = new THREE.ImageUtils.loadTexture('images/grass.jpg');
	floorTexture.wrapS = floorTexture.wrapT = THREE.RepeatWrapping;
	floorTexture.repeat.set(75, 75);

	floorMaterial = new THREE.MeshPhongMaterial({ map: floorTexture, side: THREE.DoubleSide });
	var floorGeometry = new THREE.PlaneBufferGeometry(10000, 10000);
	floor = new THREE.Mesh(floorGeometry, floorMaterial);
	floor.position.y = -0.1;
	floor.rotation.x = Math.PI / 2;
	floor.receiveShadow = true;
	scene.add(floor);

	// Materials and textures
	defaultMaterial = new THREE.MeshLambertMaterial();
	unlitMaterial = new THREE.MeshLambertMaterial({color: 0x7777ff});
	duckTexture = new THREE.ImageUtils.loadTexture('images/duckfeathers.jpg');
	duckTexture.repeat.set(.1, .1);
	var lavaTexture = new THREE.ImageUtils.loadTexture( 'images/lava3.jpg');
	duckMaterial = new THREE.MeshPhongMaterial({map:duckTexture});

	var noiseTexture = new THREE.ImageUtils.loadTexture( 'images/cloud.png' );
	noiseTexture.wrapS = noiseTexture.wrapT = THREE.RepeatWrapping; 

	// texture to additively blend with base image texture
	var blendTexture = new THREE.ImageUtils.loadTexture( 'images/lava.jpg' );
	blendTexture.wrapS = blendTexture.wrapT = THREE.RepeatWrapping; 

	var bumpTexture = noiseTexture;
	bumpTexture.wrapS = bumpTexture.wrapT = THREE.RepeatWrapping; 

	// Create custom, bumpy lava material
	bumpyMaterial = new THREE.ShaderMaterial({
		uniforms: {
			baseTexture: 	{ type: "t", value: lavaTexture },
			baseSpeed:		{ type: "f", value: 0.02 },
			repeatS:		{ type: "f", value: 1.0 },
			repeatT:		{ type: "f", value: 1.0 },
			noiseTexture:	{ type: "t", value: bumpTexture },
			noiseScale:		{ type: "f", value: 0.5 },
			blendTexture:	{ type: "t", value: blendTexture },
			blendSpeed: 	{ type: "f", value: 0.01 },
			blendOffset: 	{ type: "f", value: 0.25 },
			bumpTexture:	{ type: "t", value: bumpTexture },
			bumpSpeed: 		{ type: "f", value: 0.15 },
			bumpScale: 		{ type: "f", value: 15.0 },
			alpha: 			{ type: "f", value: 1.0 },
			time: 			{ type: "f", value: 1.0 }
		}
	});

	// Load shader files for bumpy lava material
	shaderFiles = ['glsl/bumpy.vs.glsl', 'glsl/bumpy.fs.glsl',];
	new THREE.SourceLoader().load(shaderFiles, function(shaders) {
		bumpyMaterial.vertexShader = shaders['glsl/bumpy.vs.glsl'];
		bumpyMaterial.fragmentShader = shaders['glsl/bumpy.fs.glsl'];

		bumpyMaterial.needsUpdate = true;
	})

	// Create geometry for misc objets
	duckCollisionBoxGeometry = new THREE.CylinderGeometry(0, 30, 60, 50, 50, false) ;
	fireballGeometry = new THREE.SphereGeometry( 3, 32, 32 );

	generateDucks(); 

	// Listen for events
	document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	window.addEventListener('resize', resize);
	resize();

    
	// Add on-screen control panel and scene lighting
	buildOnScreenControlPanel();
	
	// add Lighting
	addDirectionalLight();

	addTitleText();

	// Build one particle system to add and remove from scene
	makeParticleSystem(); 

}

// Load duck object
function loadOBJ(file, material, evil, duckCollisionBox, scale, xRot, yRot, zRot, duckIndex) {
	// on Progress
	var onProgress = function(query) {
		if ( query.lengthComputable ) {
			var percentComplete = query.loaded / query.total * 100;
		}
	};
	
    // on Error 
    var onError = function() {
    	console.log('Failed to load ' + file);
    };

    // create an Object loader
    var loader = new THREE.OBJLoader()
	// load in the file and objet
	
	
	loader.load(file, function(object) {
		console.log("LOAD COMPLETE", object)
		
		object.traverse(function(child) {
			if (child instanceof THREE.Mesh) {
				child.material = material;
				child.castShadow = true;
			}
		});

		object.rotation.x= xRot;
		object.rotation.y = yRot;
		object.rotation.z = zRot;
		object.scale.set(scale,scale,scale);
		object.parent = floor;

		// add duck object as child to it's collision box and then add collision box to scene
		duckCollisionBox.add(object);
		ducks.push(duckCollisionBox);
		ducks[duckIndex].evil = evil;
		scene.add(ducks[duckIndex]);
	}, onProgress, onError);
}


function generateDucks() {
	duckMaterial = new THREE.MeshPhongMaterial({map:duckTexture});

	// Generate ducks
	for (i = 0; i < numDucks ; i++){
		// generate ducks and store into array along with a boolean indicating whether a duck is good or evil
		// TODO: generate random scale and position for each duck generated
		var evil = (i % 2 === 0) ? false: true
		var duckCollisionBox = new THREE.Mesh(duckCollisionBoxGeometry, new THREE.MeshBasicMaterial({color: 0x0000ff, transparent : true, opacity: 0}));
		duckCollisionBox.evil = evil;
		
		duckCollisionBox.position.set( runOff-(Math.random()*2*runOff), 0 ,runOff-(Math.random()*2*runOff))
		ducks.push(duckCollisionBox);
		scene.add(duckCollisionBox);
		loadOBJ('obj/goose.obj', duckMaterial, evil, duckCollisionBox, 1, 0,Math.PI,0, i)
	}
}

// CHANGE MATERIAL SHADER
function updateMaterial(material, vs, fs) {
	new THREE.SourceLoader().load(shaderFiles, function(shaders) {
		material.vertexShader = shaders[vs];
		material.fragmentShader = shaders[fs];

		material.needsUpdate = true;
	})
}


// Adapt to window resize
function resize() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
}

// record location of mouse for such things as picking
function onDocumentMouseMove( event ) {
	event.preventDefault();
	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;
}

// Load duck object
function loadOBJ(file, material, evil, collisionBox, scale, xRot, yRot, zRot, duckIndex) {
	var onProgress = function(query) {
		if ( query.lengthComputable ) {
			var percentComplete = query.loaded / query.total * 100;
			console.log( Math.round(percentComplete, 2) + '% downloaded' );
		}
	};

	var onError = function() {
		console.log('Failed to load ' + file);
	};

	var loader = new THREE.OBJLoader()
	loader.load(file, function(object) {
		object.traverse(function(child) {
			if (child instanceof THREE.Mesh) {
				child.material = material;
				child.castShadow = true;
			}
		});

		object.rotation.x= xRot;
		object.rotation.y = yRot;
		object.rotation.z = zRot;
		object.scale.set(scale,scale,scale);

		// Add duck object as child to it's collision box and then add collision box to scene
		collisionBox.add(object);
	}, onProgress, onError);
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function requestAnimFrame() {

	requestAnimationFrame(render);

    // if first time checking time, set lastTimeCheck to current time
    if (!lastTimeCheck) {
    	lastTimeCheck = Date.now();
    	fps = 0;
    	fpscount = 0; 
    }
    // else, calculate fps based on time elapsed
    else { 
    	fpscount = fpscount + 1; 
    	changeInTime = Date.now() - lastTimeCheck;

    	fps = parseInt(1/(changeInTime / 1000)); 
    	totalFPS = totalFPS + fps;  
    	lastTimeCheck = Date.now();
    }
    
    
    // FPS AND NUM DUCK CAUGHT DISPLAY UPDATE
    document.getElementById("ducks_caught_display").innerHTML = "Ducks Caught:   " + duckCount;

    // update fps only if certain time has elapsed 
    if (Date.now() - startTime > timeElapsed) {
        document.getElementById("fps_display").innerHTML = "FPS:   " + (totalFPS/fpscount);
        fpscount = 0; 
        totalFPS = 0; 
        startTime = Date.now(); 
 }
        
    //WEAPON DISPLAY UPDATE 
        if (currWeapon == "breadcrumbs") {
          document.getElementById("weapon_img").innerHTML = "<img src='images/bread.png' width = '50'>"; 
    }
        if (currWeapon == "fireball") {
             document.getElementById("weapon_img").innerHTML = "<img src='images/fireball.png' width = '50'>"; 
            }

    }
////////////////////Audio/////////////////////
 function generateSound() {
     var audio = document.createElement('audio'); 
     var source = document.createElement('source');
     source.src = '/audio/ducksong.mp3';
     audio.appendChild(source);
     audio.play(); 
     }
 
/////////////////////// Make the FPS Display screens //////////////////////////////
function buildOnScreenControlPanel() {

    // Set up display to read the frames per second.
    var fpsDisplay = document.createElement('div');
    fpsDisplay.setAttribute("id", "fps_display");
    fpsDisplay.style.cssText = "position:absolute;width:290;height:100"+
    "background-color:white";
    fpsDisplay.innerHTML = "FPS:   " + "fps";
    fpsDisplay.style.top = 10;
    fpsDisplay.style.left = 10;
    document.body.appendChild(fpsDisplay);

    // Set up display to read number of ducks caught
    var ducksCaughtDisplay = document.createElement('div');
    ducksCaughtDisplay.setAttribute("id", "ducks_caught_display");
    ducksCaughtDisplay.style.cssText = "position:absolute;width:290;height:100"+
    "background-color:white";
    
    ducksCaughtDisplay.innerHTML = "Ducks Caught:   " + duckCount;
    ducksCaughtDisplay.style.top = 35;
    ducksCaughtDisplay.style.left = 10;
    document.body.appendChild(ducksCaughtDisplay);

    // weapon image display box 
    var WeaponImgDisplay = document.createElement('div');
    WeaponImgDisplay.setAttribute("id", "weapon_img");
    WeaponImgDisplay.style.cssText = "position:absolute;width:290;height:100"+
    "background-color:white";
    WeaponImgDisplay.style.top = 55;
    WeaponImgDisplay.style.left = 69;
    WeaponImgDisplay.innerHTML = "<img src='images/bread.png' width = '50'>"; 
    document.body.appendChild(WeaponImgDisplay);
    
      // weapon text  display box 
    var weaponDisplay = document.createElement('div');
    weaponDisplay.setAttribute("id", "weapon");
    weaponDisplay.style.cssText = "position:absolute;width:290;height:100"+
    "background-color:white";
    weaponDisplay.style.top = 70;
    weaponDisplay.style.left = 10;
    weaponDisplay.innerHTML = "Weapon:"; 
    document.body.appendChild(weaponDisplay);
}

// Add directional light
function addDirectionalLight() {
    
	dirLight = new THREE.DirectionalLight( 0xffffff, 1.0 );
	//dirLight.shadowCameraVisible = true;
	
	//100,70,70
	dirLight.position.set( 100, 70, 70 );

	// Enable shadow to be casted
	dirLight.castShadow = true;
	dirLight.shadowDarkness = 1;

	// create shadow box that lights target area around sphere. TODO: adjust based on number of ducks
	
	
	dirLight.shadowCameraLeft = 250;
	dirLight.shadowCameraRight = -150;
	dirLight.shadowCameraTop = 150;
	dirLight.shadowCameraBottom = -150;
	dirLight.shadowCameraNear = -159;
	dirLight.shadowCameraFar = 350;
	
	scene.add( dirLight );
	renderer.shadowMapEnabled = true;
}

// randomly move ducks
function moveDuck(){
    count = numDucks;
    
    while(count--) {
    	duck = ducks[count];

    	// ducks that are not frozen or attacking waddle around
    	if (duck != attackingDevilDuck && duck.frozen != true) {
    		if (duck.position.x < -runOff) {
    			duck.position.x = runOff-(2*runOff)*Math.random();
    		}
    		if (duck.position.z < -runOff) {
    			duck.position.z = runOff; 
    		}
    		duck.position.x -= Math.random(); 
    		duck.position.z -= Math.random(); 
    	}
    }
}

function moveDevilDuck() {
	// If duck is not frozen from being shot, duck moves towards player
	if(attackingDevilDuck != null) {

		attackingDevilDuck.rotation.y-=.01;
		attackingDevilDuck.position.add(attackingDevilDuck.chargeVector) 
		if (attackingDevilDuck.position.distanceTo(camera.position) < 25) {
			gameOver("YOU LOST");
		}
	}
}

function moveFireballs() {
	for (i = 0; i < fireballs.length ; i++){

		// get array of intersected children of the scene
		var fireballIntersects = fireballs[i].trajectory.intersectObjects( scene.children );
		
		// if current trajectory of fireball intersects transparent duck collision box, destroy duck
		if ( fireballIntersects.length > 0 && fireballIntersects[ 0 ].object.material.transparent) {
			explodeDuck(fireballIntersects[ 0 ].object, bumpyMaterial, true);
			
		}

		if (fireballs[i].distanceTravelled > 60){
			scene.remove(fireballs[i]);
			scene.remove(fireballs[i].trajectory);
			fireballs[i].geometry.dispose();
			fireballs[i].material.dispose();
			fireballs.shift();
		} else {
			
			// loop for fireball speed
			for ( j = 0; j < 10; j++) {
				fireballs[i].position.add((fireballs[i].movementVector));
			}
			fireballs[i].distanceTravelled += 1;
		}
	}
}

function explodeDuck(target, material, explode) {
		
	// set duck material to fireball material and destroy
	hitDuck = target.children[0]
	hitDuck.traverse(function(child) {
		if (child instanceof THREE.Mesh) {
			child.material = material;
		}
	})

	target.frozen = true;

	// Remove duck from scene after a time lapse
	setTimeout(function() {
		scene.remove(target);
		if (explode) {
			duckDestroyedCount++;
			attackingDevilDuck = null;
		}
		console.log(explode);
	 }, 500);		

}

// Build particle System
function makeParticleSystem(){

    // material
    particleMaterial = new THREE.ParticleBasicMaterial({color:"yellow", size:5, opacity: .5, blending: THREE.AdditiveBlending});
    
    // initialize global variable particles. particles is an array of vector positions.
    particlesGlobal = new THREE.Geometry();

        // load into single geometry, right now only one particle.    
        for (i=0; i<numParticles; i++) {
            // initialize initial position to initial position of camera TODO: START POSITION SHOULD BE CAMERA POSITION
            particle = new THREE.Vector3(camera.position.x,camera.position.y+(20-40*Math.random()),camera.position.z+(20-40*Math.random())); 
            // initialize velocity vector: this will be used to update position later
            particle.velocity = new THREE.Vector3(0,0,0);
            particlesGlobal.vertices.push(particle);
        }

        // initialize global variable particle System, mesh of particles and their material
        particleSystemGlobal = new THREE.ParticleSystem(particlesGlobal, particleMaterial);  
        // sort the particles
        particleSystemGlobal.sortParticles = true;
       
}


// CHANGE PARTICLE POSITIONS - THROW BREAD CRUMBS  
function particleChange() {
    count = numParticles;
    gravity = new THREE.Vector3(0,0,-1); 
    while (count--) {

    // extract particle to manipulate position 
    var particle = particleSystemGlobal.geometry.vertices[count];

        // if particle x position is less than -50, reset position of particle and remove system from scene
        if (particle.x <= breadCrumbDuck.position.x+20) {
        	scene.remove(particleSystemGlobal); 
        	if(breadCrumbDuck.evil == false) {
        		explodeDuck(breadCrumbDuck, unlitMaterial, false);
        		duckCount++;

        	}
        	throwCrumbs = false;
        	// Reset every particle in system back to camera position
        	particleReset();
        } else {

        // find direction vector to normal Duck
        x = (breadCrumbDuck.position.x - particle.x)+Math.random()
        y = 0;
        z = (breadCrumbDuck.position.z - particle.z)+Math.random()
        
        directionVector = new THREE.Vector3(x*.1, y, z*.1);
        particle.add(directionVector);
        particle.y-=1; 
        
    }
       // tell system update vertices
       particleSystemGlobal.geometry.verticesNeedUpdate = true;

   }
}

function particleReset() {

	// Reset each particle in system back to camera position
	for (i=0; i<numParticles; i++) {

		var particle = particleSystemGlobal.geometry.vertices[i];
		// set position of particles to position of camera
		particle.x = camera.position.x;
		particle.y = camera.position.y+(20-40*Math.random());
		particle.z = camera.position.z+(20-40*Math.random()); 
		// initialize velocity vector: this will be used to update position later
		particle.velocity = (0,0,0);
	}
}


//////////////////////////////// RENDER /////////////////////////////// 
function render() {
    
   
    
    if (duckCount == winningDuckCount) {
        gameOver("YOU WON!");
        
        }
    if (duckDestroyedCount == numDucks - duckCount) {
        console.log("duckDestroyedCount", duckDestroyedCount); 
        gameOver("YOU LOSE!"); 
        }
        
	// update position of particle system if throw crumbs
	if (throwCrumbs) {
		particleChange(); 
	}

    //particleSystemGlobal.rotation.y+=.01; 
    if (devilDuckAttack) {
    	moveDevilDuck(); 
    }

    if (fireballs.length != 0) {
    	moveFireballs();
    }
    
    moveDuck(); 
    
    raycaster.setFromCamera( mouse, camera );

	// Get array of intersected children of the scene
	var intersects = raycaster.intersectObjects( scene.children );
       // sprite.position.set(camera.position.x-100,camera.position.y,camera.position.z);
	if(currWeapon == "breadcrumbs"){

		if ( intersects.length > 0 && intersects[ 0 ].object.material.transparent) {
			// Check intersected object exists and is not already the current target
			// Check if the intersected object is a duck (has a transparent collision box)
			// Target this object
			if ( target != intersects[ 0 ].object ) {			
				if ( target) {
					// Remove spotlight from last target
					target.remove(target.spotLight);
				}
				// Set new target and set spotlight on target
				target = intersects[ 0 ].object;
				target.spotLight = new THREE.SpotLight( 0xffffff );
				target.spotLight.target = target;
				target.spotLight.angle = Math.PI/1
				target.add(target.spotLight);
				target.spotLight.position.set(0, 10, 0);
			}
		// if ray intercepts nothing, remove last target's spotlight and set target to null
		} else {
			if ( target ) {
				target.remove(target.spotLight);
			}
			target = null;
		}
	} else if ( target ) {
		target.remove(target.spotLight);
	} if ( exit == false ) {
		requestAnimFrame();
		renderer.render(scene, camera);
	}
}
		
		function onKeyDown(event) {
	        // Move forward
	        if (keyboard.eventMatches(event,"s")) {
	        	camera.translateZ(1);
	        }

	        // Move backward
	        else if (keyboard.eventMatches(event,"w")) {
	        	camera.translateZ(-1);
	        }

	       	// Strafe right
	       	else if (keyboard.eventMatches(event,"d")) {
	       		camera.translateX(1);
	       	}

	       	// Strafe left
	       	else if (keyboard.eventMatches(event,"a")) {
	       		camera.translateX(-1);
	       	}

	       	else if (keyboard.eventMatches(event,"1")) {
	       		currWeapon = "breadcrumbs";
	       	}

	       	// Strafe left
	       	else if (keyboard.eventMatches(event,"2")) {
	       		currWeapon = "fireball";
	       	}

	        // If a duck is targeted, press space to 'shoot' the duck with breadcrumbs
	        if (keyboard.eventMatches(event,"space")) {

	        	if(currWeapon == "breadcrumbs"){

	        		if (target && (!particleSystemGlobal || !particleSystemGlobal.parent)) {

        				// instantiate particle system 
						 scene.add(particleSystemGlobal);

						throwCrumbs = true ;

						// define which duck to throw crumbs at
						breadCrumbDuck = target; 

			        	// If targetted duck is an evil duck, duck attacks
			        	if (target.evil &&  attackingDevilDuck == null) {

							// devil duck attacks and you want to throw crumbs 
							devilDuckAttack = true;

							// define which duck is attacking
							attackingDevilDuck = target;

							attackingDevilDuck.chargeVector = clone(raycaster.ray.direction.negate()); 
							attackingDevilDuck.chargeVector.y = 0;
	        		
		        		} 

	        		} 
	        	} else if (currWeapon == "fireball") {
		        	// If fireball is currently weilded, create fire ball and shoot along current ray
		        	var fireball = new THREE.Mesh( fireballGeometry, bumpyMaterial );
		        	fireball.position.set(camera.position.x, camera.position.y, camera.position.z);
		        	fireball.distanceTravelled = 0;
		        	fireball.movementVector = new THREE.Vector3(raycaster.ray.direction.x, raycaster.ray.direction.y, raycaster.ray.direction.z);
		        	fireball.trajectory = new THREE.Raycaster(fireball.position, fireball.movementVector,0,5);
		        	fireballs.push(fireball);
		        	scene.add( fireball );
		        }

		        // If title text is on screen, destroy text on space
		        if(titleText.parent == camera){
		        	camera.remove(titleText)
		        	camera.remove(littleText)
		        }
		    }
		} keyboard.domElement.addEventListener('keydown', onKeyDown );

	    document.body.addEventListener('mousedown', function(event) {
	    	mouseDown = true
	    }, false);

	    document.body.addEventListener("mouseup", function(event) {
	    	mouseDown = false
	    }, false);

	    document.addEventListener('mousemove', function(event) {
	    	if (mouseDown)
	    	{
	    		mouseX = event.movementX;
	    		mouseY = event.movementY;

		        // drag left, turn right
		        if (mouseX < 0) {
		        	camera.rotateOnAxis(y_vector_norm, -1*Math.PI/100);
		        }

		  		// drag right, turn left
		  		if (mouseX > 0) {
		  			camera.rotateOnAxis(y_vector_norm, 1*Math.PI/100);
		  		}
  			}
  		}, false);

// Use to pass object by value
// Taken from stackoverflow: http://stackoverflow.com/questions/7574054/javascript-how-to-pass-object-by-value
function clone(obj){
	if(obj == null || typeof(obj) != 'object') {
		return obj;
	}

	var temp = new obj.constructor(); 
	for(var key in obj) {
		temp[key] = clone(obj[key]);
	}	

	return temp;
}

function addTitleText(){
	loader.load( 'fonts/helvetiker_bold.typeface.js', function ( font ) {

		var textGeometry = new THREE.TextGeometry( "What The Duck", {

			font: font,

			size: window_height/100,
			height: 10,
			curveSegments: 12,

			bevelThickness: .5,
			bevelSize: .5,
			bevelEnabled: true

		});

		var littleTextGeometry = new THREE.TextGeometry( "Press SPACE to START & to SHOOT", {

			font: font,

			size: window_height/400,
			height: 10,
			curveSegments: 12,

			bevelThickness: 0.01,
			bevelSize: 0.01,
			bevelEnabled: true

		});

		var textMaterial = new THREE.MeshPhongMaterial( { color: 0xff0000, specular: 0xffffff } );

		titleText = new THREE.Mesh( textGeometry, textMaterial );
		littleText = new THREE.Mesh( littleTextGeometry, textMaterial );

		titleText.position.set(-(5*(window_height/100)),0,-60);
		titleText.rotation.y = Math.PI / 70;

		littleText.position.set(-(5*(window_height/150)),-4,-60);
		littleText.rotation.y = Math.PI / 30;

		camera.add( titleText );
		camera.add( littleText );

	} );   
}


function gameOver(gameOverText) {
    loader.load( 'fonts/helvetiker_bold.typeface.js', function ( font ) {

		var textGeometry = new THREE.TextGeometry( gameOverText, {

			font: font,

			size: window_height/100,
			height: 10,
			curveSegments: 12,

			bevelThickness: .5,
			bevelSize: .5,
			bevelEnabled: true

		});

		var textMaterial = new THREE.MeshPhongMaterial( { color: "blue", specular: 0xffffff } );

		gameOverText = new THREE.Mesh( textGeometry, textMaterial )

		gameOverText.position.set(-(5*(window_height/150)),0,-60);
		gameOverText.rotation.y = Math.PI / 70;
		
		camera.add( gameOverText );
	})

    // TODO
	//exit = true;
    
}


render();